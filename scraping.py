# -*- coding: utf-8 -*-
from requests import get
from datetime import datetime
from os.path import exists
import os


"""
		CAUTION
Some errors could be find in find_price()
It's because some modifications can be applied 
In source code of item monitored
"""

def date_check(file, url, tag = ""):
	#This funtion must be improve to handle not only the date
	#but the time to, and standardize the datetime format from ISO 8601
	
	refresh = 1 #number of days before send a new request
	actual_date = 0
	if not(exists(file)):
		print("New request")
		request(file, url, tag)
		actual_date = str(datetime.now().strftime('%Y%m%d'))
	
#==========================================================================	
	#https://www.codingem.com/how-to-read-the-last-line-of-a-file-in-python/
	#Section : Find the Last Line of a Large File in Python
			
	with open(file, "rb") as f:
		try:
			f.seek(-2, os.SEEK_END)
			while f.read(1) != b'\n':
				f.seek(-2, os.SEEK_CUR)

		except OSError:
			f.seek(0)
		last_line = f.readline().decode()
#==========================================================================
		
	if (exists(file) and last_line == ""): 
		with open(file, 'a') as f:
			actual_date = f.write(str(datetime.now().strftime('%Y%m%d')))
			f.close()
		
	elif (int(datetime.now().strftime('%Y%m%d')) - refresh >= int(last_line)):
		print("New request")
		request(file, url, tag)
		#actual_date = f.write(str(datetime.now().strftime('%Y%m%d')))
		
	else:
		actual_date = str(datetime.now().strftime('%Y%m%d'))
		
	return actual_date



def request(file, url, tag):
	response = ""
	response = get(str(url)+str(tag)).text
	
	with open(file, 'w') as f:
		f.write(response)
		f.write("\n" + str(datetime.now().strftime('%Y%m%d')))
		f.close()



def find_price(file, url = "", tag = ""):

	source = ""
	position = 0
	price = ""
	casted_price = 0.0

	with open(file, 'r') as f:
		source = f.read()
		
	for i, c in enumerate(source):	
		if source[i - 86 : i] == "<aside>\n            \n    \n    \n\n                <div class=\"price\"><div class=\"price\">":
			position = i - 1
			
		if c in ['0','1','2','3','4','5','6','7','8','9' ] and i > position and position != 0:
			price = price + c
			
		if c == '€' and i > position and position != 0:
			price = price + '.'
			
		if c == '\n' and i > position and position != 0:
			break
			
	casted_price = float(price)
	return casted_price
	
def TopAchat(file):
	pass



def LDLC(file):
	store = False
	suffixe = ""
	source = ""
	
	with open(file, 'r') as f:
		source = f.read()
		
	for i, c in enumerate(source):
		if source[i : i + 6] == "fiche/":
			store = True
			
		if store == True and source[i : i + 2] == "\">":
			store = False
			break
			
		if store == True:
			suffixe = suffixe + c
			
	return "https://www.ldlc.com/"+suffixe
			
	
	
def Materiel(file):
	pass


#===========================Main===========================#


urls = ["https://www.ldlc.com/recherche/","https://www.materiel.net/recherche/","https://www.topachat.com/accueil/index.php"]
tags = ["DARK BASE PRO 900 Orange rev. 2", "DARK BASE PRO 900 Black rev. 2", "DARK BASE PRO 900 Silver rev. 2"]

price = [0]
item = [""]
date_custom = [""]
website = [""]

last_link = ""

for i, url in enumerate(urls):
	for ii, tag in enumerate(tags):
	
		if "ldlc" in url:
			file = "sources/ldlc/"+tag
			date_check(file+".txt", url, tag)
			last_link = LDLC(file+".txt")
			
			date_custom.append(date_check(file+"[Price].txt", last_link))
			price.append(find_price(file+"[Price].txt"))
			website.append("ldlc")
			item.append(tag)
			
		elif "materiel" in url:
			file = "sources/materiel/"+tag
			date_check(file+".txt", url, tag)
			last_link = Materiel(file+".txt")
			
			#date_custom.append(date_check(file+"[Price].txt", last_link))
			#price.append(find_price(file+"[Price].txt"))
			#website.append("materiel")
			#item.append(tag)
			
		elif "topachat" in url:
			file = "sources/topachat/"+tag
			date_check(file+".txt", url, tag)
			
			last_link = TopAchat(file+".txt")
			#date_custom.append(date_check(file+"[Price].txt", last_link))
			#price.append(find_price(file+"[Price].txt"))
			#website.append("topachat")
			#item.append(tag)
			
		else: 
			print("Unknows domain name")


print(website)
print(date_custom)
print(price)
print(item)

#Save the price evolution in a Excel file
#Now, thinking to run this script at the startup of OS
#Verifiing if all funtions are dynamics
#Handle the empty function
#Learn how to store the tabs date in SQL database (Replace Excel by SQL)
#Handle unavailable website
